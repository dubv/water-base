//
//  Helper.swift
//  WATERBASE
//
//  Created by Gone on 1/9/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

struct Helper {
    static func getViewController<T: UIViewController> (named: String, inSb sbNamed: String ) -> T? {
        return UIStoryboard(name: sbNamed, bundle: nil).instantiateViewController(withIdentifier: named) as? T
    }
}
