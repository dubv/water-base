//
//  Item.swift
//  WATERBASE
//
//  Created by Gone on 1/9/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import Foundation
import Firebase

class GroceryItem {
    
    let ref: DatabaseReference?
    let key: String
    let name: String
    let addedByUser: String
    var completed: Bool
    var avatar: String
//    var coords: String?
    
    init(name: String, addedByUser: String, completed: Bool, avatar: String, key: String = "") {
        self.ref = nil
        self.key = key
        self.name = name
        self.addedByUser = addedByUser
        self.completed = completed
        self.avatar = avatar
//        self.coords = coords
        
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let name = value["name"] as? String,
            let addedByUser = value["addedByUser"] as? String,
            let completed = value["completed"] as? Bool,
            let avatar = value["avatar"] as? String else {
                return nil
        }
        
        self.ref = snapshot.ref
        self.key = snapshot.key
        self.name = name
        self.addedByUser = addedByUser
        self.completed = completed
        self.avatar = avatar
    }
    
    func toAnyObject() -> Any {
        return [
            "name": name,
            "addedByUser": addedByUser,
            "completed": completed,
            "avatar": avatar
        ]
    }
}

class Coords {
    var lot: String?
    var not: String?
    
    init(lot: String, not: String) {
        self.lot = lot
        self.not = not
    }
    
    init?(snapshot: DataSnapshot) {
        guard
            let value = snapshot.value as? [String: AnyObject],
            let lot = value["lot"] as? String,
            let not = value["not"] as? String else {
                return nil
        }
        self.lot = lot
        self.not = not
    }
    
    func toAnyObject() -> Any {
        return [
            "lot": lot,
            "not": not
        ]
    }
}
