//
//  ViewController.swift
//  WATERBASE
//
//  Created by Gone on 1/9/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase
import Photos
import AudioToolbox
import MobileCoreServices

class HomeViewController: BaseViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var waterTableView: UITableView!
    @IBOutlet weak var imagePick: UIImageView!
    
    // MARK: - Properties
    var ref: DatabaseReference!
    var items: [GroceryItem] = []
    var user: User!
    let imagePicker = UIImagePickerController()
    var urlImage: String?
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
        ref = Database.database().reference(withPath: "item")
        
        setupUser()
        waterTableView.delegate = self
        waterTableView.dataSource = self
        imagePicker.delegate = self
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        
        getData()
        register()
        setupNavItems()
    }
    
    // MARK: - Initialization
    static func viewController() -> HomeViewController? {
        return Helper.getViewController(named: "HomeViewController", inSb: "Main")
    }
    
    func register() {
        waterTableView.register(KokoTableViewCell.nib, forCellReuseIdentifier: KokoTableViewCell.indentifier)
    }
    
    func setupUser() {
        Auth.auth().addStateDidChangeListener() { auth, user in
            guard let user = user else { return }
            self.user = User(authData: user)
        }
    }
    
    func setupNavItems() {
        let rightItem = UIBarButtonItem(title: "Add", style: .plain, target: self, action: #selector(addItem))
        let leftItem = UIBarButtonItem(title: "Sign out", style: .plain, target: self, action: #selector(signOut))
        let leftSeItem = UIBarButtonItem(title: "Upload", style: .plain, target: self, action: #selector(uploadImage))

        navigationItem.rightBarButtonItem = rightItem
        navigationItem.leftBarButtonItems = [ leftItem, leftSeItem ]
    }
    
    // MARK: - Private Method
    func getData() {
        ref.observe(.value, with: { snapshot in
            var newItems: [GroceryItem] = []

            for child in snapshot.children {
                if let snapshot = child as? DataSnapshot,
                    let groceryItem = GroceryItem(snapshot: snapshot) {
                    newItems.append(groceryItem)
                }
            }
            self.items = newItems
            self.waterTableView.reloadData()
        })
    }

    // MARK: - Target
    @objc func signOut() {
        let user = Auth.auth().currentUser!
        let onlineRef = Database.database().reference(withPath: "online/\(user.uid)")
        
        onlineRef.removeValue { (error, _) in
            if let error = error {
                let alert = UIAlertController(title: "Authentication", message: error.localizedDescription, preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default, handler: nil))
                self.present(alert, animated: true)
            }
            
            do {
                try Auth.auth().signOut()
                self.navigationController?.popViewController(animated: true)
            }catch let error {
                print("\(error)")
            }
        }
    }
    
    @objc func addItem() {
        let alert = UIAlertController(title: "Dummy Item",
                                      message: "Add an Item",
                                      preferredStyle: .alert)
        let saveAction = UIAlertAction(title: "Save", style: .default) { _ in
            guard let textField = alert.textFields?.first,
                let text = textField.text else { return }
            let urlImage = self.urlImage ?? ""
            let groceryItem = GroceryItem(name: text, addedByUser: self.user.email, completed: false, avatar: urlImage)
            self.items.append(groceryItem)
            
            let coords = Coords(lot: "Lote 1", not: "Note 1")
            
            let groceryItemRef = self.ref.child(text.lowercased())
            groceryItemRef.setValue(groceryItem.toAnyObject())
            
            let coordsItemRef = self.ref.child(text.lowercased()).child("nutritional")
            coordsItemRef.setValue(coords.toAnyObject())
        }
        
        let cancelAction = UIAlertAction(title: "Cancel",
                                         style: .cancel)
        alert.addTextField()
        alert.addAction(saveAction)
        alert.addAction(cancelAction)
        present(alert, animated: true, completion: nil)
    }
    
    @objc func uploadImage() {
        let photoAuthorizationStatus = PHPhotoLibrary.authorizationStatus()
        switch photoAuthorizationStatus {
        case .authorized:
            print("Access is granted by user")
            self.askForChooeseImageType()
            
        case .notDetermined:
            print("Permission Not Determined")
            PHPhotoLibrary.requestAuthorization({ (status) in
                if status == PHAuthorizationStatus.authorized{
                   self.askForChooeseImageType()
                }
            })
            
        case .restricted:
            print("User do not have access to photo album.")
            
        case .denied:
            print("User has denied the permission.")
        }
    }
}

extension HomeViewController: UITableViewDelegate, UITableViewDataSource {
    // MARK: -  UITableViewDataSource
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return items.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: KokoTableViewCell.indentifier, for: indexPath) as? KokoTableViewCell else { let cell = KokoTableViewCell(style: .default, reuseIdentifier: KokoTableViewCell.indentifier)
            return cell }
        
        cell.list = items[indexPath.row]
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, canEditRowAt indexPath: IndexPath) -> Bool {
        return true
    }
    
    func tableView(_ tableView: UITableView, commit editingStyle: UITableViewCell.EditingStyle, forRowAt indexPath: IndexPath) {
        if editingStyle == .delete {
            let groceryItem = items[indexPath.row]
            groceryItem.ref?.removeValue()
        }
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        guard let cell = tableView.cellForRow(at: indexPath) else {return}
        let item = items[indexPath.row]
        
        let toggledCompletion = !item.completed
        toggleCellCheckbox(cell, isCompleted: toggledCompletion)
        item.ref?.updateChildValues(["completed": toggledCompletion])
    }
    
    func toggleCellCheckbox(_ cell: UITableViewCell, isCompleted: Bool) {
        if !isCompleted {
            cell.accessoryType = .none
            cell.textLabel?.textColor = .black
            cell.detailTextLabel?.textColor = .black
        } else {
            cell.accessoryType = .checkmark
            cell.textLabel?.textColor = .gray
            cell.detailTextLabel?.textColor = .gray
        }
    }
    
    // MARK: - UITableViewDelegate
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableView.automaticDimension
    }
    
    func tableView(_ tableView: UITableView, estimatedHeightForRowAt indexPath: IndexPath) -> CGFloat {
        return 50
    }
}
