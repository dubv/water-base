//
//  AuthenticationViewController.swift
//  WATERBASE
//
//  Created by Gone on 1/9/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import Firebase

class SignInViewController: BaseViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var signInButton: UIButton!
    @IBOutlet weak var signUpButton: UIButton!
    
    // MARK: - Life Cycle
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Initialization
    static func viewController() -> SignInViewController? {
        return Helper.getViewController(named: "SignInViewController", inSb: "Main")
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        checkState()
    }
    
    // MARK: - Public Method
    func checkState() {
        Auth.auth().addStateDidChangeListener() { auth, user in
            if user != nil {
                self.emailTextField.text = nil
                self.passwordTextField.text = nil
                self.toHome()
            }
        }
    }
    
    // MARK: - IBAction
    @IBAction func signIn(_ sender: Any) {
        guard
            let email = emailTextField.text,
            let password = passwordTextField.text,
            email.count > 0,
            password.count > 0
            else { return }
        
        Auth.auth().signIn(withEmail: email, password: password) { user, error in
            if let error = error, user == nil {
                let alert = UIAlertController(title: "Sign In Failed",
                                              message: error.localizedDescription,
                                              preferredStyle: .alert)
                
                alert.addAction(UIAlertAction(title: "OK", style: .default))
                self.present(alert, animated: true)
            } else {
                self.toHome()
            }
        }
    }
    
    @IBAction func signUp(_ sender: Any) {
        guard let athViewController = SignUpViewController.viewController() else {return}
        pushViewController(viewController: athViewController)
    }
}
