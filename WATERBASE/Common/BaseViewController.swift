//
//  BaseViewController.swift
//  WATERBASE
//
//  Created by Gone on 1/9/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class BaseViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func pushViewController(viewController: UIViewController) {
        navigationController?.pushViewController(viewController, animated: true)
    }
    
    func toHome() {
        guard let homeViewController = HomeViewController.viewController() else {return}
        self.pushViewController(viewController: homeViewController)
    }
}
