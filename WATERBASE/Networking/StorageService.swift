//
//  StorageService.swift
//  WATERBASE
//
//  Created by Gone on 1/10/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit
import FirebaseStorage

struct StorageService {
    static func uploadImage(_ image: UIImage, path: String, completion: @escaping (URL?) -> Void) {
        
        let path = path
//        guard let uid = Auth.auth().currentUser?.uid else { return }
        let storageRef = Storage.storage().reference().child(path)
        
        guard let imageData = image.jpegData(compressionQuality: 0.75) else { return }
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"
        
        storageRef.putData(imageData, metadata: metaData) { metaData, error in
            if error == nil, metaData != nil {
                
                storageRef.downloadURL { url, error in
                    completion(url)
                }
            } else {
                return completion(nil)
            }
        }
    }
}
