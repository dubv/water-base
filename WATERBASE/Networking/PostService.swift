//
//  StorageService.swift
//  WATERBASE
//
//  Created by Gone on 1/10/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import Foundation
import UIKit
import FirebaseStorage
import Firebase

struct PostService {
    static func uploadImage(_ image: UIImage, completion: @escaping (URL?) -> Void) {
        guard let uid = Auth.auth().currentUser?.uid else { return }
        let storageRef = Storage.storage().reference().child("photo/\(uid)")

        guard let imageData = image.jpegData(compressionQuality: 0.75) else { return }
        let metaData = StorageMetadata()
        metaData.contentType = "image/jpg"

        storageRef.putData(imageData, metadata: metaData) { metaData, error in
            if error == nil, metaData != nil {

                storageRef.downloadURL { url, error in
                    completion(url)
                }
            } else {
                return completion(nil)
            }
        }
    }
//
//    static func create(for image: UIImage,path: String, completion: @escaping (String?) -> ()) {
//        let filePath = path
//
//        let imageRef = Storage.storage().reference().child(filePath)
//        StorageService.uploadImage(image, at: imageRef) { (downloadURL) in
//            guard let downloadURL = downloadURL else {
//                print("Download url not found or error to upload")
//                return completion(nil)
//            }
//            completion(downloadURL.absoluteString)
//        }
//    }
}
