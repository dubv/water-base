//
//  KokoTableViewCell.swift
//  WATERBASE
//
//  Created by Gone on 1/9/19.
//  Copyright © 2019 Gone. All rights reserved.
//

import UIKit

class KokoTableViewCell: UITableViewCell {

    // MARK: - IBOutlet
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var userLabel: UILabel!
    
    // MARK: - Initialization
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    // MARK: - Properties
    var list: GroceryItem! {
        didSet {
            guard let data = list else { return }
            nameLabel.text = data.name
            userLabel.text = data.addedByUser
            
            if data.completed == true {
                self.accessoryType = .checkmark
            }
        }
    }
    
    // MARK: - Public Method
    static var indentifier: String {
        return String(describing: self)
    }
    
    static var nib: UINib {
        return UINib(nibName: indentifier, bundle: nil)
    }
}
